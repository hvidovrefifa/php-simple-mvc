<?php
define('DEVELOPMENT_ENVIRONMENT', true);

const CONFIG = [
    'DB_SERVER' => 'localhost',
    'DB_USER' => 'root',
    'DB_PASSWORD' => '',
    'DB_DATABASE' => 'simplemvc',
];

define('SITE_BASE','http://simplemvc.test/');

define('APP_SESSION_ID', 'xeFLcX4gfj1h7WM6Yfrl');