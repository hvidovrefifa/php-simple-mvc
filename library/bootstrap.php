<?php

function  autoload($class)
{
    $directories = [
        'library',
        'application'
    ];

    foreach ($directories as $directory) {
        $file = ROOT . DS . $directory . DS . str_replace('\\','/',$class) . '.php';
        if (file_exists($file)) {
            require_once($file);
        }
    }


}

spl_autoload_register('autoload');

require_once( ROOT . DS .  'config' . DS . 'config.php');

$app = new core\Application();
$app->setReporting();
$app->removeMagicQuotes();
$app->run($url);