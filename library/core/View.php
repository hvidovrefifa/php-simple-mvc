<?php

namespace core;

class View
{

    private $controller = "";
    private $action = "";
    private $params = [];

    public function __construct($controller){
        $this->controller = $controller;
    }

    public function setAction($action)
    {
        $this->action = $action;
    }

    public function setParams($name,$value){
        $this->params[$name] = $value;
    }

    public function render($main = true, $scripts = ""){
        extract($this->params);
        include ROOT . DS . 'application' . DS . 'views' . DS . $this->controller. DS . $this->action . ".php";
    }
}