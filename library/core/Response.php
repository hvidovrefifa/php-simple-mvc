<?php

namespace core;
class Response
{

    public function setResponseHeader($code, $title)
    {
        header("HTTP/1.0 $code $title");
        echo "<h1>404 Not found</h1>";
        echo "The page that you were looking for could not be found.";
        exit();
    }
}