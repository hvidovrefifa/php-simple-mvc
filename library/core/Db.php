<?php

namespace core;

class Db
{
    private $_connection;

    public function __construct(array $CONFIG){

        try {
            $this->_connection = new \PDO("mysql:host=" . $CONFIG["DB_SERVER"]. ";dbname=". $CONFIG["DB_NAME"], $CONFIG["DB_USER"], $CONFIG["DB_PASS"]);
         $this->_connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch (\PDOException $e){
            trigger_error("Failed connection to database:" . $e->getMessage(), E_USER_ERROR);
        }
    }

    /**
     * @return \PDO pdo connection
     */
    public function getConnection()
    {
        return $this->_connection;
    }

    /**
     * @param string $table
     * @param array $arrFields
     * @return array the resultset
     */
    public function getAll(string $table, array $arrFields)
    {
        if(!empty($arrFields)){
            $fields = implode(',', $arrFields);
            $sqlQuery = "SELECT $fields FROM $table";
} else {
            $sqlQuery = "SELECT * FROM $table";
        }
        $connection = $this->getConnection();
        $statement = $connection->prepare($sqlQuery);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);

    }

    /**
     * @param string $table
     * @param array $arrFields
     * @param string $key
     * @param int $value
     * @return mixed
     */
    public function getOne(string $table, array $arrFields, string $key, int $value)
    {
        if(!empty($arrFields)){
            $fields = implode(',', $arrFields);
            $sqlQuery = "SELECT $fields FROM $table WHERE $key = :value";
        } else {
            $sqlQuery = "SELECT * FROM $table WHERE $key = :value";
        }
        $connection = $this->getConnection();
        $statement = $connection->prepare($sqlQuery);
        $statement->bindValue(":value", $value, \PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * @param string $table
     * @param array $data
     * @param $key
     * @param $value
     * @return true
     */
    public function save(string $table, array $data, $key = null, $value = null)
    {
        $sqlQuery = "$table SET";

        foreach ($data as $k => $v) {
            $sqlQuery .= " $k = :$v,";
        }

        $sqlQuery = rtrim($sqlQuery, ",");

        if($key){
            $sqlQuery = "UPDATE " . $sqlQuery . " WHERE " . $key . " = :value";
        } else {
            $sqlQuery = "INSERT INTO " . $sqlQuery;
        }

        $connection = $this->getConnection();
        $statement = $connection->prepare($sqlQuery);

        if($key){
            $statement->bindValue(":value", $value);
        }
        $statement->execute();

        return true;
    }

    /**
     * @param string $table
     * @param array $data
     * @param $key
     * @param $value
     * @return void
     */
    public function update(string $table, array $data, $key = null, $value = null)
    {
        $this->save($table, $data, $key, $value);
    }

    public function insert($table, array $data)
    {
        $this->save($table, $data);
    }

    /**
     * @param string $table
     * @param string $key
     * @param int $value
     * @return bool
     */
    public function delete(string $table, string $key , int $value)
    {
        $connection = $this->getConnection();
        $statement = $connection->prepare("DELETE FROM $table WHERE $key = :value");
        $statement->bindValue(":value", $value, \PDO::PARAM_INT);

        if($statement->execute()){
            return true;
        }

        return false;
    }
}