<?php

namespace core;

class Application
{

    public function setReporting()
    {
        if(DEVELOPMENT_ENVIRONMENT){
            ini_set('display_errors', 'on');
        } else {
            error_reporting(E_ALL);
            ini_set('display_errors', 'off');
            ini_set('error_log', ROOT . DS.'tmp'. DS. 'logs' . DS . 'error.log');
        }
    }

    public function stripSlashesDeep($value)
    {
        (is_array($value)) ? array_map([$this, 'stripSlashesDeep'], $value) : stripslashes($value);
        return $value;
    }

    public function removeMagicQuotes()
    {
        $_GET = $this->stripSlashesDeep($_GET);
    }

    public function run($url)
    {
        $url = explode("/", $url);

        $controller = array_shift($url);
        $controllerClass = "controllers\\" . ucwords($controller) . 'Controller';
        $action = array_shift($url);
        $queryString = $url;

        try {

            if(class_exists($controllerClass)){
                $parents = class_parents($controllerClass);
                if(in_array('core\Controller', $parents)){
                    if(method_exists($controllerClass, $action)){
                        $dispatch = new $controllerClass($controller, $action);

                        call_user_func_array([$dispatch, $action],$queryString);

                    } else {
                        // Bad action
                        throw  new ResourceNotFoundException('Not Found', 404);
                    }
                } else {
                    // Bad controller
                    throw  new ResourceNotFoundException('Not Found', 404);
                }
            } else {
                // Bad controller
                throw  new ResourceNotFoundException('Not Found', 404);
            }

        } catch (ResourceNotFoundException $e){
            $response = new Response();
            $response->setResponseHeader("404", "Not Found");
        }


    }

}