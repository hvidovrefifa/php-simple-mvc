<?php

namespace core;

class Model
{

    protected $table;
    protected $db;
    protected $key;
    protected $data;

    public function __construct($table){
        $this->table = $table;
        $this->db = new Db(CONFIG);
    }

    public function all($arrFields = [])
    {
        return $this->db->getAll($this->table, $arrFields);
    }

    private function find($id, $arrFields = [])
    {
        $this->db->getOne($this->table,$arrFields,'id' ,$id);
    }

    public function load($data){
        foreach ($data as $key => $value) {
            $this->data[$key] = $this->$key = $value;
        }
    }

    public function loadModel($id)
    {
        $result = $this->find($id, []);

        if($result){
            $this->load($result);
            return $this;
        }
        return false;
    }
    public function save($id = null){
        if($id){
            $result = $this->db->update($this->table, $this->data, 'id' , $id);
        } else {
            $result = $this->db->insert($this->table, $this->data);
        }

        return $result;
    }

    public function delete($key, $value){
        return $this->db->delete($this->table, $key, $value);
    }

    public function __set($name, $value)
    {
        if(method_exists($this, 'set' . ucfirst($name))){
            $method = 'set' . ucfirst($name);
            return $this->$method($value);
        }

        return $this->$name = $value;
    }

    public function __get($name){
        if(method_exists($this, 'get' . ucfirst($name))){
            $method = 'get' . ucfirst($name);
            return $this->$method();
        }
        return $this->$name;
    }

}